package SQLGenerators;

import entitise.Entity;

public interface SQLgenerator <T extends Entity>{
        String insert(T toInsert);
        String selectAll();
        String update(T toUpdate);
        String delete(T toDelete);

}
