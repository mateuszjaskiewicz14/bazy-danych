package SQLGenerators;

import entitise.Emploeey;


public class EmploeesSQLGenerator implements SQLgenerator<Emploeey> {

    @Override
    public String insert(Emploeey toInsert) {
        return "Insert into EMPLOYEES " +
                "(id,firstName, LastName, age)"+
                "values ("
                + toInsert.getId()
                +", '"+ toInsert.getFirstName() + "'"
                +", '"+ toInsert.getLastName() + "'"
                +", "+ toInsert.getAge() + ");";
    }

    @Override
    public String selectAll() {
        return "SELECT * FROM EMPLOYEES;";
    }

    @Override
    public String update(Emploeey toUpdate) {
        StringBuilder sb =  new StringBuilder();
        sb.append("UPDATE EMPLOYEES SET firstName ='")
                .append(toUpdate.getFirstName()).append("',")
                .append("lastName='").append(toUpdate.getLastName()).append("' ,")
                .append("age =").append((toUpdate.getAge()))
                .append(" where id =").append(toUpdate.getId()).append(";");
        return sb.toString();


    }

    @Override
    public String delete(Emploeey toDelete) {
        return "DELETE FROM EMPLOYEES WHERE id =" + toDelete.getId() + ";";
    }
}
