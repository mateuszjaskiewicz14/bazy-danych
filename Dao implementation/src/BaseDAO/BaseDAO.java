package BaseDAO;

import entitise.Entity;

import java.sql.*;
import java.util.List;

public abstract class BaseDAO<T extends Entity> {

    private static final String  JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://localhost:3306/EMP";

    private static final String USER ="root";
    private static final String PASS ="Zajec1.K";

    protected String sql;
    public abstract void insert(T toInsert);
    public abstract List<T> select();
    public abstract void  update(T toUpdate);
    public abstract void delete(T toDelete);
    protected abstract List<T> parse(ResultSet rs);

    protected void execute(){
        try {
            Connection konn = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement stmt = konn.createStatement();

            stmt.execute(sql);

            stmt.close();
            konn.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    protected List<T> executeSelect() {
        try {
            Connection konn = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement stmt = konn.createStatement();

            ResultSet resultSet = stmt.executeQuery(sql);
            List<T> resulit = parse(resultSet);

            resultSet.close();
            stmt.close();
            konn.close();
            return resulit;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }
}
