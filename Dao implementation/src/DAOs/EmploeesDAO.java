package DAOs;

import BaseDAO.BaseDAO;
import SQLGenerators.EmploeesSQLGenerator;
import entitise.Emploeey;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmploeesDAO extends BaseDAO<Emploeey> {
    EmploeesSQLGenerator sqlGenerator = new EmploeesSQLGenerator();
    @Override
    public void insert(Emploeey toInsert) {
        sql = sqlGenerator.insert(toInsert);
        execute();
    }

    @Override
    public List<Emploeey> select() {
        sql = sqlGenerator.selectAll();
        return executeSelect();

    }

    @Override
    public void update(Emploeey toUpdate) {
        sql = sqlGenerator.update(toUpdate);
        execute();
    }

    @Override
    public void delete(Emploeey toDelete) {
        sql = sqlGenerator.delete(toDelete);
        execute();
    }
    @Override
    protected List<Emploeey> parse(ResultSet rs){
        ArrayList<Emploeey> result = new ArrayList<>();

        try{
            while (rs.next()){
                int id = rs.getInt("id");
                String firsttName = rs .getString("firstName");
                String lastName = rs.getString("lastName");
                int age = rs. getInt("age");

                result.add(new Emploeey(id,firsttName,lastName,age));
            }

        }catch (SQLException e){
                e.printStackTrace();
        }
        return result;
    }


}
