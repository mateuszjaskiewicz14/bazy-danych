import java.util.Collection;

public class Country {
    private int population;
    private String country;

    public Country(int population, String country){
        this.country = country;
        this.population = population;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


}
