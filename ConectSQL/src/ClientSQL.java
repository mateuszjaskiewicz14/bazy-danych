import java.sql.*;
import java.util.Properties;

public class ClientSQL {

    private final String dbms = "mysql";
    private final String userName = "root";
    private final String serverName = "localhost";
    private final int portNumber = 3306;
    private final String databaseName = "world";
    private final String password = "Zajec1.K";

    public static void main(String[] args) throws SQLException {
        ClientSQL clientSQL = new ClientSQL();


        Connection connection = clientSQL.getConnection();


        clientSQL.testConnection(connection);
    }
    private Connection getConnection() throws SQLException {

        Properties connectionProps = new Properties();
        connectionProps.put("user", this.userName);
        connectionProps.put("password", this.password);


        String connectionString = "jdbc:" + this.dbms + "://" +
                this.serverName +
                ":" + this.portNumber + "/";
        System.out.println(connectionString);

        Connection conn = DriverManager.getConnection(
                connectionString,
                connectionProps);


        return conn;
    }
    private void testConnection(Connection con) throws SQLException {
        Statement stmt = null;

        String query = String.format("SELECT name FROM %s.country;", databaseName);
        System.out.println(query);
        //PreparedStatement
        try {
            stmt = con.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);


            while (resultSet.next()) {
                String countryName = resultSet.getString(1);
                System.out.println(countryName);
            }



        } catch (SQLException e ) {
            System.out.println(e.getErrorCode());
        } finally {
            if (stmt != null) { stmt.close(); }
        }
    }
    }






