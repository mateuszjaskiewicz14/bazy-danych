package com.example.demo.reoosytorys;
import com.example.demo.etietis.Course;
import com.example.demo.etietis.Review;
import com.example.demo.etietis.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Transactional
public class ReviewReposytory {
    @Autowired
    EntityManager em;
    Logger logger = LoggerFactory.getLogger(this.getClass());

    public Review findById(long id) {
        return em.find(Review.class, id);
    }

    public void deleteById(long id) {
        Review reviewToDelete = findById(id);
        em.remove(reviewToDelete);
    }

    public Review save(Review review) {
        if (review.getId() == null) {
            em.persist(review);
        } else {
            em.merge(review);
        }
        return review;
    }

    public void playWEMR() {
        logger.info("Jezdeś w metodzie");

        Review review = new Review("Smyndryl", "aslikusudgvf");
        em.persist(review);
        review.setRating("Kląb");
        review.setDescription("ASDFAASDFASDF");
        em.flush();

        Review reviewx = new Review("Grab", "golec");
        em.persist(reviewx);
        em.flush();
        logger.info("Byłeź w metodzie");

        em.detach(reviewx);
        review.setRating("grab->grabina");
    }

    public void addReviewsCourse(Long courseId, List<Review> reviews) {
        Course course = em.find(Course.class, 10003L);
        for (Review rv : reviews) {
            course.addRv(rv);
            rv.setCourse(course);
            em.persist(rv);
        }
    }
}
