package com.example.demo.etietis;

import javax.persistence.*;

@Entity

public class Passport {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
     String number;
    @OneToOne(mappedBy = "passport")// wskazuje na to skąd ma pobrać id dla danego pola
    private Student student;

    protected Passport(){}
    public Passport(String number) {
        this.number = number;
    }

    public Long getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public String toString() {
        return "Passport{" +
                "id=" + id +
                ", number='" + number + '\'' +
                '}';
    }
}
