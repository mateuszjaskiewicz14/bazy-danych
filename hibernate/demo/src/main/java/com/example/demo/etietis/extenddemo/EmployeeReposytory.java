package com.example.demo.etietis.extenddemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.swing.text.html.parser.Entity;

@Repository
@Transactional
public class EmployeeReposytory {
    @Autowired
    EntityManager entityManager;

    public Employee getById(long id){
        return entityManager.find(Employee.class,id);
    }
    public Employee save(Employee toSave){
        if (toSave.getId()== null){
            entityManager.persist(toSave);
        }else {
            entityManager.merge(toSave);
        }
        return toSave;
    }
    public void deleteById(long id){
        entityManager.remove(getById(id));
    }
}
