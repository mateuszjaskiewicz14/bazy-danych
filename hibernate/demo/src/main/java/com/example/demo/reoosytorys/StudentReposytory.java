package com.example.demo.reoosytorys;

import com.example.demo.etietis.Course;
import com.example.demo.etietis.Passport;
import com.example.demo.etietis.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Repository
@Transactional
public class StudentReposytory {

    @Autowired
    EntityManager em;
    Logger logger = LoggerFactory.getLogger(this.getClass());

    public Student findById(long id) {
        return em.find(Student.class, id);
    }

    public void deleteById(long id) {
        Student studentToDelete = findById(id);
        em.remove(studentToDelete);
    }

    public Student save(Student student) {
        if (student.getId() == null) {
            em.persist(student);
        } else {
            em.merge(student);
        }
        return student;
    }

    public void playWEM() {
        logger.info("Jezdeś w metodzie");

        Student student = new Student("Smyndryl");
        em.persist(student);
        student.setName("Kląb");
        em.flush();

        Student studentx = new Student("Grab");
        em.persist(studentx);
        em.flush();
        logger.info("Byłeź w metodzie");

        em.detach(studentx);
        student.setName("grab->grabina");
    }

    public void saveStudentWithPassoirt(){
        Passport passport = new Passport("ADALDHALLD");
        em.persist(passport);

        Student student = new Student("Krąpijj");
        student.setPassport(passport);
    }

    public void saveManyStudentsEithManyCourse(){
        Student student = new Student("Bololo");
        Student student1 = new Student("lolek");
        Course course = new Course("samotności");
        Course course1 = new Course("Inteligencja, siła , wyyrzymałość, twurczość");

    }
    public void  saveStuentWithCourse(Student student, Course course){
        em.persist(student);
        em.persist(course);
    }
}
