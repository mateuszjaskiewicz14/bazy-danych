package com.example.demo.reoosytorys;

import com.example.demo.etietis.Course;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Repository
@Transactional
public class CourseReposytory {
    @Autowired
    EntityManager em;
    Logger logger = LoggerFactory.getLogger(this.getClass());
    public Course findById(long id){
        return em.find(Course.class, id);
    }
    public void deleteById(long id){
        Course courseToDelete = findById(id);
        em.remove(courseToDelete);
    }

    public Course save(Course course){
        if(course.getId() == null){
            em.persist(course);
        }else{em.merge(course);}
        return course;
    }
    public void playWEM(){
        logger.info("Jezdeś w metodzie");

        Course course = new Course("Smyndryl");
        em.persist(course);
        course.setNameCours("Kląb");
        em.flush();

       Course coursex = new Course("Grab");
        em.persist(coursex);
        em.flush();
        logger.info("Byłeź w metodzie");

        em.detach(coursex);
        coursex.setNameCours("grab->grabina");

    }




}
