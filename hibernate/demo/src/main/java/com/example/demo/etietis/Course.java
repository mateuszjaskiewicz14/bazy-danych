package com.example.demo.etietis;

import com.sun.javafx.beans.IDProperty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "course")
@NamedQuery(name = "select_all", query = "select c from Course c")
@NamedQueries(value = {
        @NamedQuery(name = "get_all_course", query = "SELECT c FROM Course c"),
        @NamedQuery(name  = "get_all_Course_named_testowo2",
        query = "Select c from Course c where rating='testowo2'")
})
public class Course {

    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "name", nullable = false)
            private String name;
    @OneToMany(mappedBy = "course", fetch = FetchType.EAGER)
    private List<Review> rv = new ArrayList<>();
    @ManyToMany( mappedBy = "courses" )
    private List<Student> students = new ArrayList<>();



    protected Course(){}
    public Course(String nameCours){ this.name = nameCours;}

    public Long getId() {
        return id;
    }

    public String getNameCours() {
        return name;
    }

    public void setNameCours(String nameCours) {
        this.name = nameCours;
    }

   public List<Review> getRv(){
        return rv;
    }
    public void addRv(Review rv){
        this.rv.add(rv);
    }

    public List<Student> getStudents() {
        return students;
    }
    public void addStudent(Student student){
        students.add(student);
    }
    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", nameCours='" + name + '\'' +
                '}';
    }

}
