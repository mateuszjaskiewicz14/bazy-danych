package com.example.demo;

import com.example.demo.etietis.Course;
import com.example.demo.etietis.Review;
import com.example.demo.etietis.Student;
import com.example.demo.etietis.extenddemo.EmployeeReposytory;
import com.example.demo.etietis.extenddemo.FullTimeEmployee;
import com.example.demo.etietis.extenddemo.PartTimeEmployee;
import com.example.demo.reoosytorys.CourseReposytory;
import com.example.demo.reoosytorys.PassportReposytory;
import com.example.demo.reoosytorys.ReviewReposytory;
import com.example.demo.reoosytorys.StudentReposytory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.spi.LoggerFactoryBinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.ArrayList;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	CourseReposytory courseReposytory;
	@Autowired
	StudentReposytory studentReposytory;
	@Autowired
	ReviewReposytory reviewReposytory;
	@Autowired
	PassportReposytory passportReposytory;
	@Autowired
	EmployeeReposytory employeeReposytory;
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	@Override
	public void run(String...args) throws Exception{
		ArrayList<Review> list =new ArrayList<>();
		list.add(new Review("4","oko"));
		list.add(new Review("2","noga"));

		reviewReposytory.addReviewsCourse(10001L,list);

		Student student=new Student("laksdfh");
		Course course = new Course("laksdfh");

		studentReposytory.saveStuentWithCourse(student,course);

		FullTimeEmployee zbyszek = new FullTimeEmployee("Zbyszek", new BigDecimal(4000));
		PartTimeEmployee mateusz = new PartTimeEmployee("Mateusz", new BigDecimal(0));
		employeeReposytory.save(zbyszek);
		employeeReposytory.save(mateusz);

	}


}
