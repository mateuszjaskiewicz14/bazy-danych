package com.example.demo.reoosytorys;
import com.example.demo.etietis.Passport;
import com.example.demo.etietis.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Repository
@Transactional
public class PassportReposytory {
    @Autowired
    EntityManager em;
    Logger logger = LoggerFactory.getLogger(this.getClass());

    public Passport findById(long id) {
        return em.find(Passport.class, id);
    }

    public void deleteById(long id) {
        Passport passportToDelete = findById(id);
        em.remove(passportToDelete);
    }

    public Passport save(Passport passport) {
        if (passport.getId() == null) {
            em.persist(passport);
        } else {
            em.merge(passport);
        }
        return passport;
    }

    public void playWEP() {
        logger.info("Jezdeś w metodzie");

        Passport passport = new Passport("Smyndryl");
        em.persist(passport);
        passport.setNumber("Kląb");
        em.flush();

        Passport passportx = new Passport("Grab");
        em.persist(passportx);
        em.flush();
        logger.info("Byłeź w metodzie");

        em.detach(passportx);
        passport.setNumber("grab->grabina");
    }
}
