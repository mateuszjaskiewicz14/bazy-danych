package com.example.demo;

import com.example.demo.etietis.Course;
import com.example.demo.etietis.Review;
import com.example.demo.reoosytorys.CourseReposytory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class CoursReposytoryTest {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    CourseReposytory reposytory;
    @Autowired
    EntityManager em;

    @Test
    public void findbyIdTest() {
        Course course = reposytory.findById(10001L);
        Assert.assertEquals("Testowo", course.getNameCours());
    }

    @Test
    @DirtiesContext
    public void deleteByIdTest() {
        reposytory.deleteById(10002L);
        Assert.assertNull(reposytory.findById(1002L));
    }

    @Test
    @DirtiesContext
    public void save_updateTest() {
        Course course = reposytory.findById(10003L);
        Assert.assertEquals("Testowo3", course.getNameCours());

        course.setNameCours("Testowo-Aqq");
        reposytory.save(course);

        Course coursec = reposytory.findById(10003L);
        Assert.assertEquals("Testowo-Aqq", coursec.getNameCours());
    }

    @Test
    @DirtiesContext
    public void saveTest() {
        Course course = reposytory.findById(1);
        Assert.assertNull(course);

        Course course1 = new Course("Dziabong");
        reposytory.save(course1);

        Course course2 = reposytory.findById(1);
        Assert.assertEquals("Dziabong", course2.getNameCours());

    }

    @Test
    @DirtiesContext
    public void playWEM() {
        reposytory.playWEM();
    }

    @Test
    @DirtiesContext
    public void getCourseWithReviews() {
        Course course = em.find(Course.class, 10001L);
        logger.info("KURS -> {}", course);
        logger.info("OCENY -> {}", course.getRv());
    }

    @Test
    @DirtiesContext
    public void getCourseVIAReviews() {
        Review review = em.find(Review.class, 30001L);
        logger.info("OCENA -> {}", review);
        logger.info("KURS -> {}", review.getCourse());


    }
}
