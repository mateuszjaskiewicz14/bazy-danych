package com.example.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class JPQLTest {
    @Autowired
    EntityManager em;
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Test
    public void petAllCoursesTest(){
        List resoult = em.createQuery( "SELECT c FROM Course c").getResultList();
        logger.info("Wyniiki ",resoult );

    }
    @Test
    public void inName3(){
        List resoult = em.createQuery( "SELECT c FROM Course c WHERE c.name LIKE '%3%'").getResultList();
        logger.info("Wyniiki ",resoult );

    }
    @Test
    public void inNameTest(){
        List resoult = em.createQuery( "SELECT c FROM Course c  WHERE c.name LIKE 'Test%'").getResultList();
        logger.info("Wyniiki ",resoult );

    }
    @Test
    public void inNameUpper(){
        List resoult = em.createQuery( "SELECT c FROM Course c  WHERE c.id between 10003 and 10005 ").getResultList();
        logger.info("Wyniiki ",resoult );

    }@Test
    public void inNameBetw(){
        List resoult = em.createQuery( "SELECT upper(c.name) FROM Course c ").getResultList();
        logger.info("Wyniiki ",resoult );

    }
}
