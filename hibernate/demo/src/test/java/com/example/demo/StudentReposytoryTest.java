package com.example.demo;

import com.example.demo.etietis.Course;
import com.example.demo.etietis.Passport;
import com.example.demo.etietis.Student;
import com.example.demo.reoosytorys.CourseReposytory;
import com.example.demo.reoosytorys.StudentReposytory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class StudentReposytoryTest {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    StudentReposytory reposytory;
    @Autowired
    EntityManager em;

    /*@Test
    public void findbyIdTest(){
        Student student = reposytory.findById(10001L);
        Assert.assertEquals("Testowo",student.getId());
    }
    @Test
    @DirtiesContext
    public void deleteByIdTest(){
        reposytory.deleteById(10002L);
        Assert.assertNull(reposytory.findById(1002L));
    }
    @Test
    @DirtiesContext
    public void save_updateTest(){
        Course course = reposytory.findById(10003L);
        Assert.assertEquals("Testowo3", course.getNameCours());

        course.setNameCours("Testowo-Aqq");
        reposytory.save(course);

        Course coursec = reposytory.findById(10003L);
        Assert.assertEquals("Testowo-Aqq", coursec.getNameCours());
    }

    @Test
    @DirtiesContext
    public void saveTest(){
        Course course = reposytory.findById(1);
        Assert.assertNull(course);

        Course course1 = new Course("Dziabong");
        reposytory.save(course1);

        Course course2 =reposytory.findById(1);
        Assert.assertEquals("Dziabong",course2.getNameCours());

    }*/

    @Test
    @DirtiesContext
    public void playWEM(){
        reposytory.playWEM();
    }


    @Test
    public void getStudentWithPassportTest(){
        Student student = reposytory.findById(20001L);
        logger.info("STUDENT -> {}",student);
        logger.info("PASSPORT -> {}",student);
    }

    @Test
    @Transactional
    public void getStudentViaPassportTest(){
        Passport passport = em.find(Passport.class,40001L);
        logger.info("PASSPORT -> {}",passport);
        logger.info("STUDENT -> {}",passport.getStudent());
    }
    @Test
    @Transactional
    public void getStudentAndAllCourses(){
        Student student = em.find(Student.class, 20001L);
        logger.info("student -> {}", student);
        logger.info("courses -> {}", student.getCourses());
    }
}

