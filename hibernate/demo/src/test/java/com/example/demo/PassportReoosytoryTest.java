package com.example.demo;

import com.example.demo.etietis.Course;
import com.example.demo.etietis.Passport;
import com.example.demo.reoosytorys.CourseReposytory;
import com.example.demo.reoosytorys.PassportReposytory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class PassportReoosytoryTest {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    PassportReposytory reposytory;
    @Autowired
    EntityManager em;

    @Test
    public void findbyIdTest(){
        Passport passport = reposytory.findById(10001L);
        Assert.assertEquals("Testowo",passport.getNumber());
    }
    @Test
    @DirtiesContext
    public void deleteByIdTest(){
        reposytory.deleteById(10002L);
        Assert.assertNull(reposytory.findById(1002L));
    }
    @Test
    @DirtiesContext
    public void save_updateTest(){
        Passport passport = reposytory.findById(10003L);
        Assert.assertEquals("Testowo3", passport.getNumber());

        passport.setNumber("Testowo-Aqq");
        reposytory.save(passport);

        Passport passport1 = reposytory.findById(10003L);
        Assert.assertEquals("Testowo-Aqq", passport1.getNumber());
    }

    @Test
    @DirtiesContext
    public void saveTest(){
        Passport passport = reposytory.findById(1);
        Assert.assertNull(passport);

        Passport passport1 = new Passport("Dziabong");
        reposytory.save(passport1);

        Passport passport2 =reposytory.findById(1);
        Assert.assertEquals("Dziabong",passport2.getNumber());

    }

    @Test
    @DirtiesContext
    public void playWE(){
        reposytory.playWEP();
    }
}
